/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author thanh
 */
public class Diem {
    private SinhVien sinhvien;
    private MonHoc monHoc;
    private float diem;

    public SinhVien getSinhvien() {
        return sinhvien;
    }

    public void setSinhvien(SinhVien sinhvien) {
        this.sinhvien = sinhvien;
    }

    public MonHoc getMonHoc() {
        return monHoc;
    }

    public void setMonHoc(MonHoc monHoc) {
        this.monHoc = monHoc;
    }

    public float getDiem() {
        return diem;
    }

    public void setDiem(float diem) {
        this.diem = diem;
    }

    public Diem(SinhVien sinhvien, MonHoc monHoc, float diem) {
        this.sinhvien = sinhvien;
        this.monHoc = monHoc;
        this.diem = diem;
    }
    
}
