/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;

/**
 *
 * @author thanh
 */
public class SinhVien {
    private int MaSV;
    private String HoTen;
    private String DiaChi;
    private Date NgaySinh;
    private Lop Lop;
    
    public SinhVien(){}
    public SinhVien(int MaSV, String HoTen, String DiaChi, Date NgaySinh, Lop Lop) {
        this.MaSV = MaSV;
        this.HoTen = HoTen;
        this.DiaChi = DiaChi;
        this.NgaySinh = NgaySinh;
        this.Lop = Lop;
    }

    public int getMaSV() {
        return MaSV;
    }

    public void setMaSV(int MaSV) {
        this.MaSV = MaSV;
    }

    public String getHoTen() {
        return HoTen;
    }

    public void setHoTen(String HoTen) {
        this.HoTen = HoTen;
    }

    public String getDiaChi() {
        return DiaChi;
    }

    public void setDiaChi(String DiaChi) {
        this.DiaChi = DiaChi;
    }

    public Date getNgaySinh() {
        return NgaySinh;
    }

    public void setNgaySinh(Date NgaySinh) {
        this.NgaySinh = NgaySinh;
    }

    public Lop getLop() {
        return Lop;
    }

    public void setLop(Lop Lop) {
        this.Lop = Lop;
    }
}
