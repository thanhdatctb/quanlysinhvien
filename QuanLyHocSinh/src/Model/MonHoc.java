/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author thanh
 */
public class MonHoc {
    private int maMonHoc;
    private String tenMonHoc;
    private int soDV;
    private LoaiMon loaiMon;

    public MonHoc(int maMonHoc, String tenMonHoc, int soDV, LoaiMon loaiMon) {
        this.maMonHoc = maMonHoc;
        this.tenMonHoc = tenMonHoc;
        this.soDV = soDV;
        this.loaiMon = loaiMon;
    }

    public int getMaMonHoc() {
        return maMonHoc;
    }

    public void setMaMonHoc(int maMonHoc) {
        this.maMonHoc = maMonHoc;
    }

    public String getTenMonHoc() {
        return tenMonHoc;
    }

    public void setTenMonHoc(String tenMonHoc) {
        this.tenMonHoc = tenMonHoc;
    }

    public int getSoDV() {
        return soDV;
    }

    public void setSoDV(int soDV) {
        this.soDV = soDV;
    }

    public LoaiMon getLoaiMon() {
        return loaiMon;
    }

    public void setLoaiMon(LoaiMon loaiMon) {
        this.loaiMon = loaiMon;
    }
    
    
    
}
