/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Diem;
import Model.LoaiMon;
import Model.MonHoc;
import Model.SinhVien;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author thanh
 */
public class DiemController {
    private FileController fileController = new FileController();
    public void ThemDiem(Diem diem){
        String noiDung = diem.getMonHoc().getMaMonHoc()+","+diem.getSinhvien().getMaSV()+","+diem.getDiem();
        this.fileController.WriteToFile("bangdiem", noiDung);
    }
    public void ThemDiem(int masv, int maMh, int diem){
        String noiDung = masv+","+maMh+","+diem;
        this.fileController.WriteToFile("bangdiem", noiDung);
    }
    public List<Diem> getAllDiem() throws ParseException{
        List<Diem> diems = new ArrayList<Diem>();
        try {
            File myObj = new File("bangdiem.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] part = data.split(",");
                int MaSv = Integer.parseInt(part[0].trim());
                int MaMH = Integer.parseInt(part[1].trim());
                int Diem = Integer.parseInt(part[2].trim());
                SinhVien sv = new SinhVienController().getById(MaSv);
                MonHoc mh = new MonHocController().getMonHocTheoId(MaMH);
                Diem diem = new Diem(sv, mh, Diem);
                diems.add(diem);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return diems;
    }
    public List<Diem> sortByHoTenSinhVien() throws ParseException{
        List<Diem> diems = this.getAllDiem();
        Collections.sort(diems, new SortByTenSinhVien());
        return diems;
    }
    public List<Diem> sortByTenMonHoc() throws ParseException{
        List<Diem> diems = this.getAllDiem();
        Collections.sort(diems, new SortByMonHoc());
        return diems;
    }
    
    public List<Diem> getDiemTongKet() throws ParseException{
        List<Diem> diemTongKet = new ArrayList<Diem>();
        List<Diem> diems = this.getAllDiem();
        List<SinhVien> svs = new SinhVienController().getAllSinhVien();
        for(SinhVien s: svs){
            double tongdiem = 0;
            int tongDv = 0;
            for(Diem d: diems){
                if(s.getMaSV() == d.getSinhvien().getMaSV()){
                    tongdiem+= d.getDiem();
                    tongDv += d.getMonHoc().getSoDV();
                }
                
            }
            Diem tk = new Diem(s, null, (float) (tongdiem/tongDv));
            diemTongKet.add(tk);
        }
        return diemTongKet;
    }
}
