/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.LoaiMon;
import Model.MonHoc;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author thanh
 */
public class MonHocController {
    private FileController fileController = new FileController();
    public void writeToFile(MonHoc monHoc){
        String noiDung = monHoc.getMaMonHoc()+","+monHoc.getTenMonHoc()+","+monHoc.getSoDV()+","+monHoc.getLoaiMon();
        this.fileController.WriteToFile("mh", noiDung);
    }
    public List<MonHoc> get(){
        List<MonHoc> monHocs = new ArrayList<MonHoc>();
        try {
            File myObj = new File("mh.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] part = data.split(",");
                int MaMH = Integer.parseInt(part[0].trim());
                String tenMH = part[1];
                int soDV = Integer.parseInt(part[2]);
                LoaiMon loai = LoaiMon.valueOf(part[3]);
                MonHoc mh = new MonHoc(MaMH, tenMH, soDV, loai);
                monHocs.add(mh);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return monHocs;
    }
    public MonHoc getMonHocTheoId(int id){
        MonHoc monHoc = null;
        List<MonHoc> monHocs= this.get();
        for(MonHoc m: monHocs){
            if(m.getMaMonHoc()==id){
                return m;
            }
        }
        return monHoc;
    }
}
