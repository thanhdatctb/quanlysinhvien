/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Lop;
import Model.SinhVien;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author thanh
 */
public class SinhVienController {

    private FileController fileController = new FileController();

    public void themSinhVien(SinhVien sinhVien) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String ngaysinh = simpleDateFormat.format(sinhVien.getNgaySinh());
        String noiDung = sinhVien.getMaSV() + "," + sinhVien.getHoTen() + "," + sinhVien.getDiaChi() + "," + ngaysinh + "," + sinhVien.getLop();
        this.fileController.WriteToFile("SV", noiDung);
    }

    public List<SinhVien> getAllSinhVien() throws ParseException {
        List<SinhVien> sinhViens = new ArrayList<SinhVien>();
        try {
            File myObj = new File("sv.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] part = data.split(",");
                int MaSV = Integer.parseInt(part[0].trim());
                String HoTen = part[1];
                String DiaChi = part[2];
                String pattern = "yyyy-MM-dd";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                Date  NgaySinh =simpleDateFormat.parse(part[3]);
                Lop lop = Lop.valueOf(part[4]);
                SinhVien sv = new SinhVien(MaSV, HoTen, DiaChi, NgaySinh, lop);
                sinhViens.add(sv);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return sinhViens;
    }
    public SinhVien getById(int id) throws ParseException{
        SinhVien sv = new SinhVien();
        List<SinhVien> sinhViens = this.getAllSinhVien();
        for(SinhVien s: sinhViens){
            if(s.getMaSV() == id){
                return s;
            }
        }
        return sv;
    }
}
